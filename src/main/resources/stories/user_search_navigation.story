User Navigation Story

Narrative:
In order to buy sporting goods
As a customer
I want to be able to access and search items in the web store

Scenario: The customer should be able to search for specific brands and products
Given the user access the NetShoes home page
When the user searches for the product Nike
Then the search results are displayed
And only items corresponding to the search term Nike are displayed

Scenario: The product search result page should display more than 5 items
Given the user access the NetShoes home page
When the user searches for the product Nike
Then the search results are displayed
And more than 5 items should be displayed in the result

Scenario: The customer should be able to add items in the shopping cart
Given the user access the NetShoes home page
When the user searches for the product Nike
Then the search results are displayed
When the user adds 2 products in the shopping cart
And the user clicks on the shopping cart icon
Then the shopping cart is displayed
And the shopping cart contains all products added by the user
And the total price should be the sum of the individual prices of each item

Scenario: The customer should be able to calculate the shipping cost in the shopping cart
Given the user access the NetShoes home page
When the user searches for the product Nike
Then the search results are displayed
When the user adds 1 products in the shopping cart
And the user clicks on the shopping cart icon
Then the shopping cart is displayed
And the user informs the CEP 83035-350 in the Frete field
And the user clicks on Calcular Frete
Then the shipping cost FRETE GRATIS should be displayed to the user


Scenario: The customer should be able to clear the shopping cart
Given the user access the NetShoes home page
When the user searches for the product Nike
Then the search results are displayed
When the user adds a product in the shopping cart
And the user clicks on the shopping cart icon
Then the shopping cart is displayed
And the shopping cart contains all products added by the user
When the user clicks on Limpar Carrinho
Then all added items are removed from the shopping cart