User signup Story

Narrative:
In order to buy sporting goods
As a customer
I want to be able to signup to Netshoes online story

Scenario: The customer should be able to navigate to the web store home page
Given the customer access the NetShoes home page
When the customer clicks on Login
Then the login/signup page shows up
When the customer inserts the email@email.com on 'Criar conta' field
Then the signup pages loads
When fills all the fields with his valid information
Then the customer is redirected to the home page
And his name appears on top right of the home page
