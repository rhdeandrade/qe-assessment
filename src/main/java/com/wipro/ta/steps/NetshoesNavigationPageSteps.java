package com.wipro.ta.steps;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.wipro.ta.pages.CartPage;
import com.wipro.ta.pages.HomePage;
import com.wipro.ta.pages.SearchResultsPage;
import org.apache.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.junit.experimental.theories.Theory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsEmptyCollection.empty;

/**
 * Created by e068806 on 7/10/2018.
 */
@Component
public class NetshoesNavigationPageSteps {

    @Autowired
    private HomePage homePage;

    @Autowired
    private SearchResultsPage searchResultsPage;

    @Autowired
    private CartPage cartPage;

    @Value("${home.url}")
    private String NETSHOES_HOMEPAGE_URL;

    protected Logger LOG = Logger.getLogger(this.getClass());

    @Autowired
    protected WebDriverProvider webDriverProvider;

    @Given("the user access the NetShoes home page")
    public void givenUserAccessHomePage() {
        LOG.info("Navigating user to page: " + NETSHOES_HOMEPAGE_URL);
        webDriverProvider.get().get(NETSHOES_HOMEPAGE_URL);
    }

    @When("the user searches for the product $productName")
    public void whenSearchesForProduct(@Named("productName") String productName) {
        homePage.getSearchField().sendKeys(productName);
        homePage.getSearchField().sendKeys(Keys.RETURN);
        searchResultsPage.setSearchTeam(productName);
    }

    @Then("the search results are displayed")
    public void assertPageResultsDisplayed() {
        assertThat(searchResultsPage.getResults(), is(not(empty())));
    }

    @Then("only items corresponding to the search term $productName are displayed")
    public void assertOnlyNikeProductsAreDisplayed(@Named("productName") final String productName) {
        boolean allMatch = FluentIterable.from(searchResultsPage.getResults()).allMatch(new Predicate<WebElement>() {
            @Override
            public boolean apply(WebElement webElement) {
                return webElement.getText().contains(productName);
            }
        });

        assertThat(allMatch, is(true));
    }

    @Then("more than $count items should be displayed in the result")
    public void assertNumberOfItemsDisplayed(@Named("count") String count) {
        assertThat(searchResultsPage.getResults(), hasSize(greaterThan(Integer.valueOf(count))));
    }

    @When("the user adds $count products in the shopping cart")
    public void whenUserAddsItemsToCart(@Named("count") String count) {
        searchResultsPage.addItems(Integer.valueOf(count));
        cartPage.setProductsAddedCount(Integer.valueOf(count));
    }

    @When("the user clicks on the shopping cart icon")
    public void whenUserClicksOnShoppingCart() {

        homePage.goCart();
    }

    @Then("the shopping cart is displayed")
    public void assertShoppingCartIsDisplayed() {
        assertThat(cartPage.getShoppingItems(), is(notNullValue()));
        assertThat(cartPage.getTotalPrice(), is(notNullValue()));
    }

    @Then("the shopping cart contains all products added by the user")
    public void assertShoppingCartContainsProducts() {
        assertThat(cartPage.getShoppingItems(), hasSize(equalTo(cartPage.getProductsAddedCount())));
    }

    @Then("the total price should be the sum of the individual prices of each item")
    public void assertTotalPriceIstheSumOfIndividualItems() {
        String totalPrice = cartPage.getTotalPrice().getText();
        totalPrice = totalPrice.substring(totalPrice.indexOf(" ")).replaceAll(",", ".").trim();

        assertThat(BigDecimal.valueOf(Double.valueOf(totalPrice)).doubleValue(), equalTo(cartPage.sumPrices().doubleValue()));
        cartPage.cleanCart();
    }

    @Then("the user informs the CEP $cep in the Frete field")
    public void userInsertsCEPForShipping(@Named(value = "cep") String cep) {
        cartPage.insertShippingAddress(cep);
    }

    @Then("the user clicks on Calcular Frete")
    public void calculateShipping() {
        cartPage.calculateShipping();
    }

    @Then("the shipping cost FRETE GRATIS should be displayed to the user")
    public void assertIsFreeShipping() {
        assertThat(cartPage.isFreeShipping(), equalTo(true));
    }

    @When("the user adds a product in the shopping cart")
    public void userAddsItemToCart() {
        searchResultsPage.addItems(1);
        cartPage.setProductsAddedCount(1);
    }

    @When("the user clicks on Limpar Carrinho")
    public void userClicksOnClearCart() {
        cartPage.cleanCart();
    }

    @Then("all added items are removed from the shopping cart")
    public void assertCartIsEmpty() {
        assertThat(cartPage.getEmptyCartDiv().isDisplayed(), is(equalTo(true)));
    }
}
