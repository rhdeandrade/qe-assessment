package com.wipro.ta.steps;

import com.wipro.ta.pages.HomePage;
import org.apache.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.web.selenium.WebDriverProvider;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

@Component
public class NetshoesHomePageSteps extends AbstractSteps {

    @Value("${home.url}")
    private String NETSHOES_HOMEPAGE_URL;

    @Autowired
    private HomePage homePage;

    protected Logger LOG = Logger.getLogger(this.getClass());

    @Autowired
    protected WebDriverProvider webDriverProvider;

    @Given("the customer access the NetShoes home page")
    public void givenCustomerAccessHomePage() {
        LOG.info("Navigating user to page: " + NETSHOES_HOMEPAGE_URL);
        webDriverProvider.get().get(NETSHOES_HOMEPAGE_URL);
    }

    @Then("the products content should be displayed")
    public void thenProductListIsDisplayed() {
        assertThat(homePage.getSearchButton(), is(not(nullValue())));
        assertThat(homePage.getSearchField(), is(not(nullValue())));
    }
}