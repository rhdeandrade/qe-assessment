package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.openqa.selenium.Beta;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.SystemClock;

import java.util.List;

/**
 * Created by e068806 on 7/10/2018.
 */
@PageObject
public class SearchResultsPage extends AbstractPage {

    @FindAll(@FindBy(xpath = "//div//span[@itemprop='name']"))
    private List<WebElement> results;

    @FindAll(@FindBy(xpath = "//a[@class='i card-link']"))
    private List<WebElement> itemsToShop;

    @FindBy(xpath = "//section[@class='product-size-selector']//a[@qa-option='available']")
    private WebElement sizeButton;

    @FindBy(id = "buy-button-now")
    private WebElement buyButton;

    @FindBy(id = "search-input")
    private WebElement searchField;

    @FindBy(xpath = "//button[@qa-automation='home-search-button']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@qa-automation='home-cart-button']")
    private WebElement cartButton;

    @FindBy(xpath = "//a[@qa-automation='home-button']")
    private WebElement homeButton;

    String searchTeam;


    public List<WebElement> getResults() {
        return results;
    }

    public List<WebElement> getItemsToShop() {
        return itemsToShop;
    }

    public void addItems(int count) {
        for (int i = 0; i < count; i++) {
            selectItemToShop(i);
            clickSizeButton();
            clickBuyButton();
            waitCartPageLoad();
            //more items to add
            if (i != (count - 1)) {
                searchField.sendKeys(searchTeam);
                searchField.sendKeys(Keys.RETURN);
            }
        }
    }

    public void setSearchTeam(String searchTeam) {
        this.searchTeam = searchTeam;
    }

    public WebElement getCartButton() {
        return cartButton;
    }

    public WebElement getSizeButton() {
        return sizeButton;
    }

    public WebElement getBuyButton() {
        return buyButton;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public String getSearchTeam() {
        return searchTeam;
    }

    public void selectItemToShop(int i) {
        waitForVisibility(itemsToShop.get(i));
        itemsToShop.get(i).click();
    }

    public void clickSizeButton() {

        waitForVisibility(sizeButton);

        try {
            sizeButton.click();

        } catch (WebDriverException ex) {

            waitForVisibility(sizeButton);

            sizeButton.click();

        }
    }

    public void clickBuyButton() {


        waitForVisibility(buyButton);
        try {

            buyButton.click();

        } catch (WebDriverException ex) {

            waitForVisibility(buyButton);

            buyButton.click();

        }
    }

    @Override
    public void goHome() {
        waitForVisibility(homeButton);
        homeButton.click();
    }

    public void goCart() {

        cartButton.click();
    }
}
