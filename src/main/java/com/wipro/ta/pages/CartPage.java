package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by e068806 on 7/10/2018.
 */
@PageObject
public class CartPage extends AbstractPage {

    @FindAll(@FindBy(xpath = "//p[@qa-automation='cart-product-price']"))
    private List<WebElement> shoppingItems;

    @FindBy(xpath = "//p[@qa-automation='cart-price']")
    private WebElement totalPrice;

    @FindBy(xpath = "//a[@qa-automation='home-cart-button']")
    private WebElement cartButton;

    @FindBy(xpath = "//a[@qa-automation='home-button']")
    private WebElement homeButton;

    @FindBy(className = "calculate-shipping__input")
    private WebElement shippingField;

    @FindBy(className = "calculate-shipping__button")
    private WebElement shippingButton;

    @FindBy(className = "cart-free-shipping-text")
    private WebElement freeShippingWarning;

    @FindBy(xpath = "//a[@qa-automation='cart-erase-cart-button']")
    private WebElement cleanCartButton;

    @FindBy(xpath = "//section[@class='cart-link-text cart-link-text--small'")
    WebElement sectionElement;

    @FindBy(className = "empty-cart")
    WebElement emptyCartDiv;

    private Integer productsAddedCount;

    public List<WebElement> getShoppingItems() {
        return shoppingItems;
    }

    public WebElement getTotalPrice() {
        return totalPrice;
    }

    public BigDecimal sumPrices() {
        BigDecimal d = new BigDecimal(0d);
        for(WebElement e : shoppingItems) {
            String value = e.getText().substring(e.getText().indexOf(" ")).trim();
            value.replaceAll(",", ".");
            d = d.add(BigDecimal.valueOf(Double.valueOf(value.replaceAll(",", "."))));
        }
        return d;
    }

    public void setProductsAddedCount(Integer productsAddedCount) {
        this.productsAddedCount = productsAddedCount;
    }

    public Integer getProductsAddedCount() {
        return productsAddedCount;
    }

    public WebElement getCartButton() {
        return cartButton;
    }

    public void goHome() {
        waitPageLoad();
        homeButton.click();
    }

    public void cleanCart() {
        cleanCartButton.click();
        this.productsAddedCount = 0;
    }

    public void insertShippingAddress(String cep) {
        shippingField.sendKeys(cep);
    }

    public void calculateShipping() {
        shippingButton.click();
    }

    public boolean isFreeShipping() {
        return freeShippingWarning != null;
    }

    public WebElement getEmptyCartDiv() {
        waitForVisibility(emptyCartDiv);
        return emptyCartDiv;
    }

}
