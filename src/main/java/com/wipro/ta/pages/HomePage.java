package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by e068806 on 7/9/2018.
 */
@PageObject
public class HomePage extends AbstractPage {

    @FindBy(id = "search-input")
    private WebElement searchField;

    @FindBy(xpath = "//button[@qa-automation='home-search-button']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@qa-automation='home-cart-button']")
    private WebElement cartButton;

    @FindBy(xpath = "//a[@qa-automation='home-button']")
    private WebElement homeButton;

    public WebElement getCartButton() {
        return cartButton;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    @Override
    public void goHome() {
        waitPageLoad();
        homeButton.click();
    }

    public void goCart() {
        try {
            cartButton.click();
        } catch (WebDriverException ex) {
            cartButton.click();
        }
    }
}
