package com.wipro.ta.pages;

import org.apache.log4j.Logger;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class AbstractPage {

    protected Logger LOG = Logger.getLogger(this.getClass());

    @Autowired
    protected WebDriverProvider webDriverProvider;

    public void waitPageLoad() {
        WebDriverWait wait = new WebDriverWait(webDriverProvider.get(), 6000);
        wait.until(ExpectedConditions.visibilityOfAllElements(elementsToWait()));
        wait.until(ExpectedConditions.invisibilityOfAllElements(invisibilityElementsToWait()));
    }

    public void waitCartPageLoad() {
        WebDriverWait wait = new WebDriverWait(webDriverProvider.get(), 6000);
        wait.until(ExpectedConditions.visibilityOfAllElements(elementsToWaitCart()));
        wait.until(ExpectedConditions.invisibilityOfAllElements(invisibilityElementsToWait()));
    }

    protected List<WebElement> elementsToWait() {
        return webDriverProvider.get().findElements(By.xpath("//a[@qa-automation='home-button']"));

    }

    protected List<WebElement> elementsToWaitCart() {
        List<WebElement> elements = webDriverProvider.get().findElements(By.xpath("//a[@qa-automation='home-button']"));
        elements.addAll(webDriverProvider.get().findElements(By.xpath("//p[@qa-automation='cart-product-price']")));
        elements.addAll(webDriverProvider.get().findElements(By.xpath("//p[@qa-automation='cart-price']")));

        return elements;

    }


    protected List<WebElement> invisibilityElementsToWait() {
        return webDriverProvider.get().findElements(By.className("ng-loading"));
    }

    protected void waitForVisibility(WebElement element) throws Error{
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new WebDriverWait(webDriverProvider.get(), 6000, 600).until(ExpectedConditions.visibilityOf(element));
    }

    abstract public void goHome();
}